# Translation of Phosh into Ukrainian
# Copyright (C) 2020 phosh developers
# This file is distributed under the same license as the phosh package.
#
# Roman Riabenko <roman@riabenko.com>, 2020. #zanata.
# Yuri Chornoivan <yurchor@ukr.net>, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: phosh\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/World/Phosh/phosh/issues\n"
"POT-Creation-Date: 2022-08-31 08:37+0000\n"
"PO-Revision-Date: 2022-08-31 18:48+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <trans-uk@lists.fedoraproject.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 20.12.0\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)\n"

#. Translators: this is the session name, no need to translate it
#: data/phosh.session.desktop.in.in:4
msgid "Phosh"
msgstr "Phosh"

#: data/sm.puri.Phosh.desktop.in.in:4
msgid "Phone Shell"
msgstr "Інтерфейс користувача телефону"

#: data/sm.puri.Phosh.desktop.in.in:5
msgid "Window management and application launching for mobile"
msgstr "Керування вікнами і запуск програм для мобільних"

#: plugins/calendar/calendar.desktop.in.in:4
msgid "Calendar"
msgstr "Календар"

#: plugins/calendar/calendar.desktop.in.in:5
msgid "A simple calendar widget"
msgstr "Простий віджет календаря"

#: plugins/upcoming-events/upcoming-events.desktop.in.in:4
msgid "Upcoming Events"
msgstr "Майбутні події"

#: plugins/upcoming-events/upcoming-events.desktop.in.in:5
msgid "Show upcoming calendar events"
msgstr "Показати майбутні події календаря"

#: src/app-grid-button.c:529
msgid "Application"
msgstr "Програма"

#: src/app-grid.c:137
msgid "Show All Apps"
msgstr "Показати усі програми"

#: src/app-grid.c:140
msgid "Show Only Mobile Friendly Apps"
msgstr "Показати лише програми для мобільних"

#: src/bt-info.c:92 src/feedbackinfo.c:78 src/rotateinfo.c:103
msgid "On"
msgstr "❙"

#: src/bt-info.c:94
msgid "Bluetooth"
msgstr "Bluetooth"

#: src/docked-info.c:81
msgid "Docked"
msgstr "Зафіксовано"

#: src/docked-info.c:81 src/docked-info.c:199
msgid "Undocked"
msgstr "Від'єднано"

#: src/end-session-dialog.c:162
msgid "Log Out"
msgstr "Вийти"

#: src/end-session-dialog.c:165
#, c-format
msgid "%s will be logged out automatically in %d second."
msgid_plural "%s will be logged out automatically in %d seconds."
msgstr[0] "Вихід з системи %s буде здійснено автоматично за %d секунду."
msgstr[1] "Вихід з системи %s буде здійснено автоматично за %d секунди."
msgstr[2] "Вихід з системи %s буде здійснено автоматично за %d секунд."

#: src/end-session-dialog.c:171 src/ui/top-panel.ui:36
msgid "Power Off"
msgstr "Вимкнути"

#: src/end-session-dialog.c:172
#, c-format
msgid "The system will power off automatically in %d second."
msgid_plural "The system will power off automatically in %d seconds."
msgstr[0] "Система автоматично вимкнеться за %d секунду."
msgstr[1] "Система автоматично вимкнеться за %d секунди."
msgstr[2] "Система автоматично вимкнеться за %d секунд."

#: src/end-session-dialog.c:178 src/ui/top-panel.ui:29
msgid "Restart"
msgstr "Перезапустити"

#: src/end-session-dialog.c:179
#, c-format
msgid "The system will restart automatically in %d second."
msgid_plural "The system will restart automatically in %d seconds."
msgstr[0] "Система автоматично перезапуститься за %d секунду."
msgstr[1] "Система автоматично перезапуститься за %d секунди."
msgstr[2] "Система автоматично перезапуститься за %d секунд."

#: src/end-session-dialog.c:269
msgid "Unknown application"
msgstr "Невідома програма"

#. Translators: quiet and silent are fbd profiles names:
#. see https://source.puri.sm/Librem5/feedbackd#profiles
#. for details
#: src/feedbackinfo.c:69
msgid "Quiet"
msgstr "Тихо"

#. Translators: quiet and silent are fbd profiles names:
#. see https://source.puri.sm/Librem5/feedbackd#profiles
#. for details
#: src/feedbackinfo.c:75
msgid "Silent"
msgstr "Беззвучно"

#: src/location-manager.c:268
#, c-format
msgid "Allow '%s' to access your location information?"
msgstr "Дозволити «%s» доступ до даних щодо вашого місця перебування?"

#: src/location-manager.c:273
msgid "Geolocation"
msgstr "Геопозиціювання"

#: src/location-manager.c:274
msgid "Yes"
msgstr "Так"

#: src/location-manager.c:274
msgid "No"
msgstr "Ні"

#: src/lockscreen.c:169 src/ui/lockscreen.ui:232
msgid "Enter Passcode"
msgstr "Уведіть пароль"

#: src/lockscreen.c:392
msgid "Checking…"
msgstr "Звіряю…"

#. Translators: Used when the title of a song is unknown
#: src/media-player.c:322 src/ui/media-player.ui:182
msgid "Unknown Title"
msgstr "Невідома назва"

#. Translators: Used when the artist of a song is unknown
#: src/media-player.c:330 src/ui/media-player.ui:165
msgid "Unknown Artist"
msgstr "Невідомий виконавець"

#: src/monitor-manager.c:119
msgid "Built-in display"
msgstr "Вбудований дисплей"

#: src/monitor-manager.c:137
#, c-format
msgctxt ""
"This is a monitor vendor name, followed by a size in inches, like 'Dell 15\"'"
msgid "%s %s"
msgstr "%s %s"

#: src/monitor-manager.c:144
#, c-format
msgctxt ""
"This is a monitor vendor name followed by product/model name where size in "
"inches could not be calculated, e.g. Dell U2414H"
msgid "%s %s"
msgstr "%s %s"

#. Translators: An unknown monitor type
#: src/monitor-manager.c:153
msgid "Unknown"
msgstr "Нетиповий"

#: src/network-auth-prompt.c:201
#, c-format
msgid "Authentication type of wifi network “%s” not supported"
msgstr "Підтримки типу розпізнавання у мережі wifi «%s» не передбачено"

#: src/network-auth-prompt.c:206
#, c-format
msgid "Enter password for the wifi network “%s”"
msgstr "Введіть пароль до мережі wifi «%s»"

#: src/notifications/mount-notification.c:122
msgid "Open"
msgstr "Відкрити"

#: src/notifications/notification.c:383 src/notifications/notification.c:639
msgid "Notification"
msgstr "Сповіщення"

#. Translators: Timestamp seconds suffix
#: src/notifications/timestamp-label.c:84
msgctxt "timestamp-suffix-seconds"
msgid "s"
msgstr "с"

#. Translators: Timestamp minute suffix
#: src/notifications/timestamp-label.c:86
msgctxt "timestamp-suffix-minute"
msgid "m"
msgstr "хв"

#. Translators: Timestamp minutes suffix
#: src/notifications/timestamp-label.c:88
msgctxt "timestamp-suffix-minutes"
msgid "m"
msgstr "хв"

#. Translators: Timestamp hour suffix
#: src/notifications/timestamp-label.c:90
msgctxt "timestamp-suffix-hour"
msgid "h"
msgstr "г"

#. Translators: Timestamp hours suffix
#: src/notifications/timestamp-label.c:92
msgctxt "timestamp-suffix-hours"
msgid "h"
msgstr "г"

#. Translators: Timestamp day suffix
#: src/notifications/timestamp-label.c:94
msgctxt "timestamp-suffix-day"
msgid "d"
msgstr "д"

#. Translators: Timestamp days suffix
#: src/notifications/timestamp-label.c:96
msgctxt "timestamp-suffix-days"
msgid "d"
msgstr "д"

#. Translators: Timestamp month suffix
#: src/notifications/timestamp-label.c:98
msgctxt "timestamp-suffix-month"
msgid "mo"
msgstr "м"

#. Translators: Timestamp months suffix
#: src/notifications/timestamp-label.c:100
msgctxt "timestamp-suffix-months"
msgid "mos"
msgstr "міс"

#. Translators: Timestamp year suffix
#: src/notifications/timestamp-label.c:102
msgctxt "timestamp-suffix-year"
msgid "y"
msgstr "р"

#. Translators: Timestamp years suffix
#: src/notifications/timestamp-label.c:104
msgctxt "timestamp-suffix-years"
msgid "y"
msgstr "р"

#: src/notifications/timestamp-label.c:121
msgid "now"
msgstr "зараз"

#. Translators: time difference "Over 5 years"
#: src/notifications/timestamp-label.c:189
#, c-format
msgid "Over %dy"
msgstr "Понад %dр."

#. Translators: time difference "almost 5 years"
#: src/notifications/timestamp-label.c:193
#, c-format
msgid "Almost %dy"
msgstr "Майже %dр."

#. Translators: a time difference like '<5m', if in doubt leave untranslated
#: src/notifications/timestamp-label.c:200
#, c-format
msgid "%s%d%s"
msgstr "%s%d%s"

#: src/polkit-auth-agent.c:228
msgid "Authentication dialog was dismissed by the user"
msgstr "Користувач перервав діалог автентифікації"

#: src/polkit-auth-prompt.c:278 src/ui/gtk-mount-prompt.ui:20
#: src/ui/network-auth-prompt.ui:82 src/ui/polkit-auth-prompt.ui:56
#: src/ui/system-prompt.ui:32
msgid "Password:"
msgstr "Пароль:"

#: src/polkit-auth-prompt.c:325
msgid "Sorry, that didn’t work. Please try again."
msgstr "На жаль, це не спрацювало. Будь ласка, спробуйте ще."

#: src/rotateinfo.c:81
msgid "Portrait"
msgstr "Книжкова"

#: src/rotateinfo.c:84
msgid "Landscape"
msgstr "Альбомна"

#. Translators: Automatic screen orientation is either on (enabled) or off (locked/disabled)
#. Translators: Automatic screen orientation is off (locked/disabled)
#: src/rotateinfo.c:103 src/rotateinfo.c:186
msgid "Off"
msgstr "○"

#: src/run-command-dialog.c:129
msgid "Press ESC to close"
msgstr "Натисніть клавішу «Esc», щоб закрити"

#: src/run-command-manager.c:94
#, c-format
msgid "Running '%s' failed"
msgstr "Не вдалося запустити «%s»"

#: src/system-prompt.c:365
msgid "Passwords do not match."
msgstr "Паролі не збігаються."

#: src/system-prompt.c:372
msgid "Password cannot be blank"
msgstr "Пароль не може бути порожнім"

#: src/torch-info.c:80
msgid "Torch"
msgstr "Факел"

#: src/ui/app-auth-prompt.ui:49
msgid "Remember decision"
msgstr "Запам'ятати вибір"

#: src/ui/app-auth-prompt.ui:62 src/ui/end-session-dialog.ui:53
msgid "Cancel"
msgstr "Скасувати"

#: src/ui/app-auth-prompt.ui:71 src/ui/end-session-dialog.ui:62
msgid "Ok"
msgstr "Гаразд"

#: src/ui/app-grid-button.ui:55
msgid "App"
msgstr "Програма"

#: src/ui/app-grid-button.ui:79
msgid "Remove from _Favorites"
msgstr "Вилучити з _вибраних"

#: src/ui/app-grid-button.ui:84
msgid "Add to _Favorites"
msgstr "Додати до _вибраних"

#: src/ui/app-grid-button.ui:89
msgid "View _Details"
msgstr "Пере_глянути подробиці"

#: src/ui/app-grid.ui:21
msgid "Search apps…"
msgstr "Шукати серед програм…"

#: src/ui/end-session-dialog.ui:31
msgid "Some applications are busy or have unsaved work"
msgstr "Деякі програми зайняті або мають незбережені дані"

#: src/ui/gtk-mount-prompt.ui:94
msgid "User:"
msgstr "Користувач:"

#: src/ui/gtk-mount-prompt.ui:117
msgid "Domain:"
msgstr "Домен:"

#: src/ui/gtk-mount-prompt.ui:150
msgid "Co_nnect"
msgstr "З'єд_натися"

#: src/ui/lockscreen.ui:39 src/ui/lockscreen.ui:341
msgid "Back"
msgstr "Назад"

#: src/ui/lockscreen.ui:93
msgid "Slide up to unlock"
msgstr "Протягніть угору, щоб розблокувати"

#: src/ui/lockscreen.ui:283
msgid "Emergency"
msgstr "Екстрений виклик"

#: src/ui/lockscreen.ui:299
msgid "Unlock"
msgstr "Розблокувати"

#: src/ui/network-auth-prompt.ui:5 src/ui/polkit-auth-prompt.ui:6
msgid "Authentication required"
msgstr "Слід пройти розпізнавання"

#: src/ui/network-auth-prompt.ui:40
msgid "_Cancel"
msgstr "_Скасувати"

#: src/ui/network-auth-prompt.ui:58
msgid "C_onnect"
msgstr "З'_єднатися"

#: src/ui/polkit-auth-prompt.ui:122
msgid "Authenticate"
msgstr "Автентифікуватися"

#: src/ui/run-command-dialog.ui:6
msgid "Run Command"
msgstr "Виконати команду"

#: src/ui/settings.ui:301
msgid "No notifications"
msgstr "Немає сповіщень"

#: src/ui/settings.ui:342
msgid "Clear all"
msgstr "Очистити все"

#: src/ui/system-prompt.ui:62
msgid "Confirm:"
msgstr "Підтвердіть:"

#: src/ui/top-panel.ui:15
msgid "Lock Screen"
msgstr "Заблокувати екран"

#: src/ui/top-panel.ui:22
msgid "Logout"
msgstr "Вийти"

#. Translators: This is a time format for a date in
#. long format
#: src/util.c:340
msgid "%A, %B %-e"
msgstr "%A, %-d %B"

#: src/vpn-info.c:89
msgid "VPN"
msgstr "VPN"

#: src/widget-box.c:54
msgid "Plugin not found"
msgstr "Додаток не знайдено"

#: src/widget-box.c:57
#, c-format
msgid "The plugin '%s' could not be loaded."
msgstr "Не вдалося завантажити додаток «%s»."

#: src/wifiinfo.c:90
msgid "Wi-Fi"
msgstr "Wi-Fi"

#. Translators: Refers to the cellular wireless network
#: src/wwan-info.c:200
msgid "Cellular"
msgstr "Стільниковий"

#: plugins/upcoming-events/event-list.c:142
msgid "Today"
msgstr "Сьогодні"

#: plugins/upcoming-events/event-list.c:144
msgid "Tomorrow"
msgstr "Завтра"

#. Translators: An event/appointment is happening on that day of the week (e.g. Tuesday)
#: plugins/upcoming-events/event-list.c:147
#, c-format
msgid "On %A"
msgstr "%A"

#: plugins/upcoming-events/event-list.c:150
#, c-format
msgid "In %d days"
msgstr "За %d днів"

#: plugins/upcoming-events/event-list.ui:26
msgid "No events"
msgstr "Немає подій"

#. Translators: This is the time format used in 24-hour mode.
#: plugins/upcoming-events/upcoming-event.c:56
msgid "%R"
msgstr "%R"

#. Translators: This is the time format used in 12-hour mode.
#: plugins/upcoming-events/upcoming-event.c:59
msgid "%l:%M %p"
msgstr "%H:%M"

#. Translators: An all day event
#: plugins/upcoming-events/upcoming-event.c:122
#: plugins/upcoming-events/upcoming-event.c:159
msgid "All day"
msgstr "Увесь день"

#. Translators: When the event ends: Ends\r16:00
#: plugins/upcoming-events/upcoming-event.c:148
msgid "Ends"
msgstr "Завершується"

#: plugins/upcoming-events/upcoming-event.c:398
msgid "Untitled event"
msgstr "Подія без назви"

#~ msgid "Show only adaptive apps"
#~ msgstr "Показувати лише адаптивні програми"
